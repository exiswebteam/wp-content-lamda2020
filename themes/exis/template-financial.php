<?php
/**
 * Template Name: Section - Financial
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;

get_template_part('templates/section-header');
get_template_part('templates/content-financial');
get_template_part('templates/section-footer');
?>
