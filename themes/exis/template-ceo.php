<?php
/**
 * Template Name: Section - CEO
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;

get_template_part('templates/section-header');
get_template_part('templates/content-ceo');
get_template_part('templates/section-footer');
?>
