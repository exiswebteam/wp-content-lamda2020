/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
(function($)
{
  var EXIS_JS = {
  // All pages
  'common': {
    init: function()
    {
      //vars
      var $body = $('body');
      var $window = $(window);
      var $hero = $('.hero-header');
      var $scrollFixedNav = $('#sections-numerical-nav');
      var _showNavOn = '01';


      /* Wait window to fully load and then run functions
      ---------------------------------------------------*/
      $window.load(function()
      {
        //Site loaded
        setTimeout(function()
        {
          $body.addClass('site-loaded');
        },200);

        //Initialize scrolling effects on load
        setTimeout(function()
        {
          //Init visible element
          $('html,body').animate({scrollTop: ($window.scrollTop() + 1)});
        },1780);


        /* FN: Hero area
        ----------------------------------------------------*/
        $hero.HERO_slider();

        //Initialize animations
        setTimeout(function()
        {
          $hero.HERO_Animations();
        },1620);

        /* FN: Visible element on viewport while scrolling
        ----------------------------------------------------*/
        $body.find('.on-viewport').visibleElementFN();

        $('.slideshow-init').Slideshow_INIT();
        $('.slideshow-text').Slideshow_TEXT();

        /* FN: Chart parallax on s1
        ----------------------------------------------------*/
        $body.find('.parallax-chart').chartParallax();


        /* FN: Set up coordinates array to sections and nav
        ----------------------------------------------------*/
        $(".scroll-section").scrollSections();


        /* FN: Navigation functions
        ----------------------------------------------------*/
        //*** Active class on scroll
        $scrollFixedNav.activeClass();

        //*** hashtagScroll
        $window.hashtagScroll();

        //*** Scrolling events
        $scrollFixedNav.primaryNavScrollingEvent();

        //*** Show navigation on section 01
        $scrollFixedNav.showScrollingNav(_showNavOn);


        /* FN: Investment parallax
        ----------------------------------------------------*/
        $('.investment-content-wrap').investmentParallax(_showNavOn);

        //*** Section background
        //$('.x-section-parallax').sectionBgParallax();

        //*** Description parallax Hellinikon
        //$('.desc-parallax').descParallax();

        /* FN: Human Capital
        ----------------------------------------------------*/
        $('.human-capital-pie').HumanCapital_Pies();

        /* FN: Investment
        ----------------------------------------------------*/
        //$('.investment-pie-item').Investment_Pies();

        /* FN: sustainable icons - negative margin
        ----------------------------------------------------*/
        if($body.find('.margin-true').length)
        {
          $body.find('.margin-true').prev().addClass('margin-true-prev');
        }

        if($('.picture-card-img-wrap > img').length)
        {
          var _heightImg = parseInt($('.picture-card-img-wrap > img').height()) - 1;

          $('.picture-card-content-inner').css('min-height',_heightImg+'px');

          $window.resize(function()
          {
            var _heightResize = parseInt($('.picture-card-img-wrap > img').height()) - 1;

            $('.picture-card-content-inner').css('min-height',_heightResize+'px');
          });
        }


        //Init skrollr animations
        setTimeout(function()
        {
          if($window.width()>1023)
          {
            var EXIS_skrollr = skrollr.init();
          }
        },200);

      });//window loaded
    },
    finalize: function()
    {
      /* FN: Scroll to fn
      ----------------------------------------------------*/
      $('.scroll-to').scrollToSection();

      /* FN: Read more global
      ----------------------------------------------------*/
      $('.read-more').readMore();
    }
  },
  'home': {
    init: function()
    {
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var fire;
    var namespace = EXIS_JS;
    funcname = (funcname === undefined) ? 'init' : funcname;
    fire = func !== '';
    fire = fire && namespace[func];
    fire = fire && typeof namespace[func][funcname] === 'function';

    if (fire) {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    // Fire common init JS
    UTIL.fire('common');

    // Fire page-specific init JS, and then finalize JS
    $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
      UTIL.fire(classnm);
      UTIL.fire(classnm, 'finalize');
    });

    // Fire common finalize JS
    UTIL.fire('common', 'finalize');
  }
};

// Load Events
$(document).ready(UTIL.loadEvents);



/**************************************************************************************************/
/************************************   jQuery FUNCTIONS  *****************************************/
/**************************************************************************************************/



/*----------------------------------------------------------------------------
  global var: Parallax in Vieport - chris
----------------------------------------------------------------------------*/
var isInViewport = function(node) {
  var rect = node.getBoundingClientRect();
  return (
    (rect.height > 0 || rect.width > 0) &&
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  );
};


/*----------------------------------------------------------------------------
  FN: Parallax for chart
----------------------------------------------------------------------------*/
$.fn.chartParallax = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);

    $window.scroll(function()
    {
      var scrolled = $window.scrollTop();

      $target.each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 90);
          $(this).css(property,'-'+parseInt(-(ratio * 0.28)) + 'px');
        }
      });
    });
  }
};


/*----------------------------------------------------------------------------
  FN: Parallax for chart
----------------------------------------------------------------------------*/
$.fn.numberParallax = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);

    $window.scroll(function()
    {
      var scrolled = $window.scrollTop();

      $target.each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 90);
          $(this).css('transform','translateY(-'+parseInt((ratio * 0.28) / 100) + 'px)');
        }
      });
    });
  }
};


/*----------------------------------------------------------------------------
  FN: Section 4 Parallax - investment
----------------------------------------------------------------------------*/
$.fn.investmentParallax = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);

    $window.scroll(function()
    {
      var scrolled = $window.scrollTop();

      $('.investment-content-wrap').each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 100);
          $('.parallax-image',this).css('background-position-y','-'+parseInt((ratio * 2)) + 'px');
        }
      });


      $('.investment-content-wrap').each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 100);
          $('.parallax-description',this).css('bottom','-'+parseInt((ratio * 0.5)) + 'px');
        }
      });
    });
  }
};



/*----------------------------------------------------------------------------
  FN: Section 1
----------------------------------------------------------------------------*/
$.fn.sectionBgParallax = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);

    $window.scroll(function()
    {
      var scrolled = $window.scrollTop();

      $target.each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 100);
          $(this).css('background-position-y',''+parseInt((ratio * 1.6)) + 'px');
        }
      });
    });
  }
};




/*----------------------------------------------------------------------------
  FN: Section 1
----------------------------------------------------------------------------*/
$.fn.descParallax = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);

    $window.scroll(function()
    {
      var scrolled = $window.scrollTop();

      $target.each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 100);
          $(this).css('bottom','-'+parseInt((ratio * 2)) + 'px');
        }
      });
    });
  }
};




/*-------------------------------------------------------------------------------
      Hero Animations
-------------------------------------------------------------------------------*/
$.fn.HERO_Animations = function ()
{
  $target = $(this);

  if($target.length)
  {
    if($('.hero-slideshow').length)
    {
      TweenMax.to('.hero-slideshow',1,{
        delay: 0,
        opacity: 1,
        translateX: 0,
        ease: Expo.easeInOut
      });
    }

    TweenMax.to('.navbar-brand',1,{
      delay: 0.8,
      opacity: 1,
      translateX: 0,
      ease: Expo.easeInOut
    });

    TweenMax.to('.tw1',1.6,{
      delay: 1,
      opacity: 1,
      translateX: 0,
      ease: Expo.easeInOut
    });

    TweenMax.to('.tw2',1.6,{
      delay: 1.4,
      opacity: 1,
      scale: 1,
      ease: Expo.easeInOut
    });

    TweenMax.to('.tw3',1.6,{
      delay: 2,
      opacity: 1,
      bottom: '90px',
      scale: 1,
      ease: Expo.easeInOut
    });

    setTimeout(function()
    {
      $target.HERO_Pies();
    },2420);
  }
};



/*-------------------------------------------------------------------------------
      Hero
-------------------------------------------------------------------------------*/
$.fn.HERO_slider = function ()
{
  $target = $(this);

  if($target.length)
  {
    $target.slick({
      dots: false,
      infinite: true,
      fade: true,
      speed: 1200,
      slidesToShow: 1,
      slidesToScroll: 1,
      slide: '.slide-item',
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5600
    });
  }
};


/*-------------------------------------------------------------------------------
      Hero
-------------------------------------------------------------------------------*/
$.fn.Slideshow_TEXT = function ()
{
  $target = $(this);

  if($target.length)
  {
    $target.each(function()
    {
      $(this).slick({
        dots: false,
        infinite: true,
        fade: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        slide: '.slide-item',
        arrows: true,
        autoplay: true,
        pauseOnFocus: true,
        pauseOnHover: true,
        prevArrow: $('.arrow-prev', this),
        nextArrow: $('.arrow-next', this),
        autoplaySpeed: 2000
      });
    });

  }
};


/*-------------------------------------------------------------------------------
      Slideshow
-------------------------------------------------------------------------------*/
$.fn.Slideshow_INIT = function ()
{
  $target = $(this);

  if($target.length)
  {
    $target.each(function()
    {
      var _fade = ($(this).attr('data-fade'))? Boolean($(this).attr('data-fade')) : true;
      var _speed = ($(this).attr('data-speed'))? parseInt($(this).attr('data-speed')) : 2000;

      $(this).slick({
        dots: false,
        infinite: true,
        fade: _fade,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        slide: '.slide-item',
        arrows: false,
        autoplay: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        autoplaySpeed: _speed
      });
    });

  }
};


/*-------------------------------------------------------------------------------
      Human capital Pies
-------------------------------------------------------------------------------*/
$.fn.HumanCapital_Pies = function()
{
  $target = $(this);

  if($target.length)
  {
    var _val = '';

    $(window).scroll(function()
    {
      $('.human-capital-pie:not(.loaded)').each(function(index, element)
      {
        var visible = isInViewport(this);

        if(visible)
        {
          $(this).each(function(index, element)
          {
            if($(this).length)
            {
              _val = parseInt($(this).attr('data-val'));
              _color_1 = $(this).attr('data-color-1');
              _color_2 = $(this).attr('data-color-2');

              $(this).drawDoughnutChart(
                [{
                  value : _val,
                  color: _color_1,
                },
                {
                  value : 100 - _val,
                  color: _color_2,
                },
              ]);
            }
          }).promise().done(function(){ $(this).addClass('loaded'); });
        }
      });
    });
  }
};



/*-------------------------------------------------------------------------------
      Human capital Pies
-------------------------------------------------------------------------------*/
$.fn.Investment_Pies = function()
{
  $target = $(this);

  if($target.length)
  {
    var _val = '';

    $(window).scroll(function()
    {
      $('.investment-pie-item:not(.loaded)').each(function(index, element)
      {
        var visible = isInViewport(this);

        if(visible)
        {
          $(this).each(function(index, element)
          {
            if($(this).length)
            {
              _val = parseInt($(this).attr('data-val'));
              _color_1 = $(this).attr('data-color-1');
              _color_2 = $(this).attr('data-color-2');

              $(this).drawDoughnutChart(
                [{
                  value : _val,
                  color: _color_1,
                },
                {
                  value : 100 - _val,
                  color: _color_2,
                },
              ]);
            }
          }).promise().done(function(){ $(this).addClass('loaded'); });
        }
      });
    });
  }
};



/*-------------------------------------------------------------------------------
      Pies
-------------------------------------------------------------------------------*/
$.fn.HERO_Pies = function()
{
  $target = $(this);

  if($target.length)
  {
    var _val = '';

    if($('.hero-header > .hero-pie-init').length)
    {
      $('.hero-pie-init').each(function()
      {
        _val = parseInt($(this).attr('data-val'));

        $(this).drawDoughnutChart(
          [{
            value : _val,
            color: '#D0021B',
          },
          {
            value : 100 - _val,
            color: '#033769',
          },
        ]);
      }).promise().done(function(){ $('.pie-item').addClass('loaded'); });
    }
  }
};


/*------------------------------------------------------------------------------------
    Read More
-------------------------------------------------------------------------------------*/
$.fn.readMore = function(_offset)
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $readMore = $(this).attr('href');
      var $target_readMore = $($readMore);

      if($target_readMore.length)
      {
        if($target_readMore.hasClass('active'))
        {
          $target_readMore.slideUp().removeClass('active');
          $(this).removeClass('active').text($(this).attr('data-more'));
        } else {
          $target_readMore.slideDown().addClass('active');
          $(this).addClass('active').text($(this).attr('data-less'));
        }
      }
      else
      {
        alert('Section not found');
      }
    });

  }
};


/*------------------------------------------------------------------------------------
    Scroll to Section
-------------------------------------------------------------------------------------*/
$.fn.scrollToSection = function(_offset)
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $scroll = ($(this).attr('href'))? $(this).attr('href') : $(this).attr('data-id');
      var $targetScroll = $($scroll);

      if($targetScroll.length)
      {
        var _offset_top = ($(this).attr('data-offset'))?parseInt($(this).attr('data-offset')):0;
        var _pos = $targetScroll.offset().top-_offset_top;

        $('html, body').animate({
          scrollTop:_pos
        }, 900);
      }
      else
      {
        alert('Section not found');
      }
    });

  }
};



/*-------------------------------------------------------------------------------
    Visible Element
-------------------------------------------------------------------------------*/
$.fn.visibleElementFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    $(window).scroll(function()
    {
      $target.each(function(i, el)
      {
        var _el = $(el);

        if (_el.onViewportFN(true))
        {
          _el.addClass("element-visible");
        }
      });
    });
  }
};


/*-------------------------------------------------------------------------------
    On Viewport Element - Visible
-------------------------------------------------------------------------------*/
$.fn.onViewportFN = function(partial)
{
  var $t            = $(this),
      $w            = $(window),
      viewTop       = $w.scrollTop(),
      viewBottom    = viewTop + $w.height(),
      _top          = $t.offset().top+90,
      _bottom       = _top + $t.height(),
      compareTop    = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};



/*-------------------------------------------------------------------------------
    Show Scrolling navigation
-------------------------------------------------------------------------------*/
$.fn.showScrollingNav = function(showNavOn)
{
  var $target = $(this);

  if($target.length)
  {
    var _height = $('.scroll-section[data-section="'+showNavOn+'"]').offset().top-180,
        $body = $("body"),
        $window = $(window),
        $pos = "";

    $window.scroll(function()
    {
       $pos = $window.scrollTop();

       if($pos>=_height)
       {
         $body.addClass("nav-init");
         $target.addClass('active');
       } else {
         $body.removeClass("nav-init");
         $target.removeClass('active');
       }
    });
  }
};



/*-----------------------------------------------------------------------------------------------
  1. NAV: build coordinates array for each page - IMPORTANT: trigger when content is fully load
------------------------------------------------------------------------------------------------*/
$.fn.scrollSections = function()
{
   var $target = $(this);

   if($target.length)
   {
     var $main_nav = $("#sections-main-menu");
     var item = 0;
     var items = [];

     //Build offset coordinates and prepare to calculate
     $target.each(function()
     {
        var pos = $(this).offset();

        item = Math.round(pos.top);

        //Array
        items.push(item);

        $(this).attr("data-coordinates",item);
     })
     .promise().done(function()
     {
       var _counter = 1;

       //put coordinates on menu
       for (i=0; i<=$target.length; i++)
       {
         var _id = _counter++;

         $main_nav.find("li:eq("+i+")").attr("data-coordinates",items[i]).addClass("menu-0"+_id);
       }
     });
   }
};


/*-----------------------------------------------------------------------------------------------
  2. Event for main nav
------------------------------------------------------------------------------------------------*/
$.fn.primaryNavScrollingEvent = function()
{
  var $target = $(this);

  if($target.length)
  {
    if($target.length)
    {
      var $main_nav_links = $target.find("a");

      //EVENT for scrolling
      $main_nav_links.click(function(e)
      {
        e.preventDefault();

        var _getSection = $(this).text().toLowerCase();
        var _page = _getSection.replace(/ /g,"-");
        var $target_page = $(".scroll-section[data-section="+_page+"]");
        var $hashtag = window.location.hash;

        $target.find(".active").removeClass("active");

        $(this).parents("li").eq(0).addClass("active");

        if($target_page.length)
        {
          var $page_offset = $target_page.offset().top;

          $("body,html").animate({
            scrollTop: $page_offset+3
          },1200);

          //Remove hashtag on click
          if($hashtag.length)
          {
            window.history.pushState(null, null, ' ');
          }
        }
        else
        {
          alert("This page does not exist. Please contact us.");
        }

        return false;
      });
    }
  }
};


/*-----------------------------------------------------------------------------------------------
    2. NAV: callback after build coordinates - active class
-----------------------------------------------------------------------------------------------*/
$.fn.activeClass = function()
{
  var $target = $(this);

  if($target.length)
  {
    $(window).scroll(function()
    {
        var $sections = $(".scroll-section");
        var scroll_active = $(window).scrollTop();
        var _target = '';

        // change sub nav active
        $sections.each(function()
        {
          var _gutterSpace = 0;
          var _s = scroll_active + _gutterSpace;
          var _top = $(this).attr('data-coordinates');
          var _bottom = _top + $(this).height();

          if( _s > _top && _s < _bottom)
          {
            _target = '.menu-' + $(this).attr('data-section'); //id
            _targetColor = ($(this).attr('data-nav-color'))? $(this).attr('data-nav-color'): '';
          }
        });

        //console.log(scroll_active);
        $target.find("li").removeClass('active');
        $target.removeClass('white black blue green');

        if(_target.length)
        {
          $(_target).addClass('active');
          $target.addClass(_targetColor);
        }
    });
  }
};



/*-----------------------------------------------------------------------------------------------
   menu scroll hashtag on load
------------------------------------------------------------------------------------------------*/
$.fn.hashtagScroll = function()
{
  //Onload - Search for hashtag
  var hashtag = window.location.hash;

  if(hashtag.length)
  {
    var $get_section = window.location.hash.substr(1);
    var $target_section = $(".scroll-section[data-section="+$get_section+"]");

    if($target_section.length)
    {
      var $page_offset = $target_section.offset().top;

      $("body,html").animate({
        scrollTop: $page_offset+3
      },1200);
    }
    else
    {
      alert("This page does not exist. Please contact us.");
    }
  }

};




})(jQuery); /******** Fully reference jQuery after this point. *************/
