<?php
//FINANCIAL section
$stock_title = get_sub_field('title');
$stock_desc = get_sub_field('content');
$stock_chart_svg = get_sub_field('stock_chart_svg');
?>

<div class="inner-section-4 on-viewport pt4" data-fx="the_companys_stock">
  <header class="container ovf-hidden">
    <h4 class="section-subtitle blue x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);">
      <?= $stock_title; ?>
    </h4>
  </header>

  <div class="container ovf-hidden">
    <div class="row">
      <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
        <div class="desc tc x-s1-2" data-bottom-top="opacity:0; transform:translateX(-30%);" data-center-center="opacity: 1; transform:translateX(0);">
          <?= $stock_desc; ?>
        </div>
      </div>
    </div>
  </div>

  <div class="ovf-hidden">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 ml-auto mr-auto">
          <div class="stock-chart on-viewport" data-bottom-top="opacity:0; transform:translateX(10%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?php
            //SVG IMAGE
            if(!empty($stock_chart_svg)):
              //SVG IMAGE
              if (strpos($stock_chart_svg['url'], '.svg') !== false)
              {
                get_template_part('templates/svg/svg-financial');
              }
            endif;
            ?>

            <?php
            //Stock value
            if( have_rows('stock_data') ):
              $counter = 1;

              while ( have_rows('stock_data') ) : the_row();
                $color = get_sub_field('color');
                $val_html = get_sub_field('value');
                ?>
                <div class="data-val val-<?= $counter++; ?>" style="color: <?= $color; ?>;"><?php echo $val_html; ?></div>
              <?php
              endwhile;
            endif;
            ?>

            <nav class="stock-chart-nav">
              <?php
              //Stock value
              if( have_rows('stock_data') ):
                while ( have_rows('stock_data') ) : the_row();
                  $color = get_sub_field('color');
                  $desc = get_sub_field('description');
                  ?>
                  <div class="item">
                    <span class="square" style="background-color: <?= $color; ?>;"></span>
                    <span class="text"><?= $desc; ?></span>
                  </div>
                <?php
                endwhile;
              endif;
              ?>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div><!--.skrollr-container-->


</div><!--.inner-section-4-->
