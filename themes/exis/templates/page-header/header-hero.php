<?php
$image = get_field('image');
$title = get_field('title');
$hero_video = get_field('video_hero');

$date_svg = get_field('date_svg');
$subtitle_svg = get_field('subtitle_svg');
?>

<div class="hero-header">

  <?php if( have_rows('slideshow') ): ?>
  <div id="hero-slideshow" class="hero-slideshow">
    <?php
      while ( have_rows('slideshow') ) : the_row();
        $slide = get_sub_field('image');
        ?>
        <div class="slide-item">
          <div class="img" style="background-image: url(<?= $slide['url']; ?>);"></div>
        </div>
      <?php
      endwhile;
    ?>
  </div>
  <?php endif; ?>

  <div class="hero-title">
    <?php
    if(!empty($date_svg)):
    ?>
    <img src="<?= $date_svg['url']; ?>" class="title-svg tw2" alt="<?= get_bloginfo('name'); ?>" width="315" height="80">
    <?php
    endif;
    ?>
    <?php
    if(!empty($subtitle_svg)):
    ?>
    <img src="<?= $subtitle_svg['url']; ?>" class="subtitle-svg tw1" alt="<?= get_bloginfo('name'); ?>" width="278" height="20">
    <?php
    endif;
    ?>
  </div>


  <?php
  if(!empty($hero_video)):
  ?>
  <div class="hero-video">
    <?php
    if(!empty($title)):
    ?>
    <h1 class="hero-title asty-light"><?= $title; ?></h1>
    <?php
    endif;
    ?>

    <?= $hero_video; ?>
  </div>
  <?php
  endif;
  ?>

  <?php
  if( have_rows('add_pie') ):?>
  <div class="hero-pies">
    <section class="container">
      <?php
          echo '<div class="pie-wrap">';
          $i = 1;
          while ( have_rows('add_pie') ) : the_row();
            $pie_val = get_sub_field('value');
            $pie_title = get_sub_field('title');
            ?>
            <div class="pie-item item-<?= $i++; ?>">
              <div class="pie-wrap">
                <div class="pie hero-pie-init" data-val="<?= $pie_val; ?>"></div>
                <span class="val">
                  <span class="numscroller" data-min="0" data-max="<?= $pie_val; ?>" data-delay="1" data-increment="1"><?= '0'; ?></span>%
                </span>
              </div>
              <h2 class="tw2"><?= $pie_title; ?></h2>
            </div>
          <?php
          endwhile;

          echo '</div>';
        ?>
    </section>
  </div>
  <?php
  endif;
  ?>

  <a href="#00" class="tw3 scroll-to scroll-arrow" data-offset="0"></a>
</div>
