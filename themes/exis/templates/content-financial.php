<?php
/*
use class .financial-content-wrap for css */
?>
<div class="financial-content-wrap mb4 page-section">
  <?php
  //The content
  get_template_part('templates/section-financial/section-1');

  //Fx content area
  if( have_rows('add_section') ):

      while ( have_rows('add_section') ) : the_row();

      if( get_row_layout() == 'numerical_figures' ): get_template_part('templates/section-financial/section-2');
      elseif( get_row_layout() == 'share_capital_increase' ): get_template_part('templates/section-financial/section-3');
      elseif( get_row_layout() == 'the_companys_stock' ): get_template_part('templates/section-financial/section-4');

      //Close
      endif;

      endwhile;

    else :
      // no flexible content

  endif;

  ?>
</div><!-- .financial-content-wrap -->
