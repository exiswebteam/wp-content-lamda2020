<?php
/*
use class .sustainable-content-wrap for css */
?>
<div class="sustainable-content-wrap page-section">
  <?php
  //The content
  get_template_part('templates/section-sustainable/section-1');

  //Fx content area
  if( have_rows('add_section_sustainable') ):

      while ( have_rows('add_section_sustainable') ) : the_row();

      if( get_row_layout() == 'icons_section' ): get_template_part('templates/section-sustainable/section-2');
      elseif( get_row_layout() == 'landfill_waste_icon' ): get_template_part('templates/section-sustainable/section-3');
      elseif( get_row_layout() == 'landfill_waste_data' ): get_template_part('templates/section-sustainable/section-4');
      elseif( get_row_layout() == 'social_section' ): get_template_part('templates/section-sustainable/section-5');
      elseif( get_row_layout() == 'initiatives_section' ): get_template_part('templates/section-sustainable/section-6');
      elseif( get_row_layout() == 'values_section' ): get_template_part('templates/section-sustainable/section-7');
      elseif( get_row_layout() == 'content_section' ): get_template_part('templates/section-sustainable/section-8');

      //Close
      endif;

      endwhile;

    else :
      // no flexible content

  endif;

  ?>
</div><!-- .sustainable-content-wrap -->
