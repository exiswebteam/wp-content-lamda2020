<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$desc = get_sub_field('description');
$bg_color = get_sub_field('background_color');
?>

<div class="square-up-right sh1 square-green">
  <div class="inner-section-5 on-viewport x-ovf-hidden pt4 pb2" data-fx="social_section">
    <header class="container tc">
      <h4 class="blue mb3 x-s1-1" data-bottom-top="opacity:0.2; transform:translateY(80%);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h4>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
          <div class="desc tc mb4 x-s1-2" data-bottom-top="opacity:0.2; transform:translateX(-40%);" data-center-center="opacity: 1; transform:translateY(0);">
            <?= $desc; ?>
          </div>
        </div>
      </div>
    </div>


    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 ml-auto mr-auto">
          <div class="row social-data-repeater">
            <?php
            if( have_rows('social_data') ):
              $count_social_data = 0;
              while ( have_rows('social_data') ) : the_row();
              $icon = get_sub_field('icon');
              $title = get_sub_field('title');
              $desc = get_sub_field('subtitle');
            ?>
            <div class="col-md-4 item mb2 tc x-op-<?= $count_social_data++; ?>">
              <img class="icon" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
              <h5 class="blue title-3" data-bottom-top="opacity:0.4; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h5>
              <div class="wrap-desc desc" data-bottom-top="opacity:0.4; transform:translateY(60%);" data-center-center="opacity: 1; transform:translateY(0);">
                <?= $desc; ?>
              </div>
            </div>
            <?php
              endwhile;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .inner-section-5 -->
</div>
