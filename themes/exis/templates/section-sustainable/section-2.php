<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$ID = str_replace(' ', '', strtolower($title));
$desc = get_sub_field('description');
$icon = get_sub_field('icon');
$bg_color = get_sub_field('background_color');
$text_color = get_sub_field('text_color');
$title_color = get_sub_field('title_color');
$secondary_desc = get_sub_field('secondary_description');
$add_border = strtolower(get_sub_field('add_border'));
$columns = get_sub_field('columns');
$negative_margin = get_sub_field('negative_margin');

$cols = ($columns=='2')? '6' : '4';
?>
<div id="fx-<?= $ID; ?>" class="inner-section-2 on-viewport pt3 pb1 margin-<?= $negative_margin; ?>" data-fx="icons_section" style="background-color: <?= $bg_color; ?>;">
  <hgroup class="container tc">
    <?php if(!empty($icon)): ?>
    <h4 class="section-subtitle mb1 <?= $text_color; ?> border-<?= $add_border; ?> x-s1-1" data-bottom-top="opacity:0; transform:translateY(30%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
      <img src="<?= $icon['url']; ?>" alt="<?= $title; ?>">
    </h4>
    <?php endif; ?>
    <h3 class="text-2 mb2 <?= $title_color; ?> x-s1-2" data-bottom-top="opacity:0; transform:translateY(30%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);"><?= $title; ?></h3>
  </hgroup>

  <?php if(!empty($desc)): ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-10 col-lg-6 ml-auto mr-auto">
        <div class="desc tc mb4 s1-3" data-bottom-top="opacity:0; transform:translateX(50%);" data-center-center="opacity: 1; transform:translateX(0);">
          <?= $desc; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>


  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-10 col-lg-8 ml-auto mr-auto">
        <div class="row justify-content-md-center">
          <?php
          if( have_rows('add_icons') ):
            $count_icons_items = 0;
            while ( have_rows('add_icons') ) : the_row();
            $title = get_sub_field('title');
            $subtitle = get_sub_field('subtitle');
          ?>
          <article class="col-md-<?= $cols; ?> item mb2 tc x-op-<?= $count_icons_items++; ?>" data-bottom-top="opacity:0; transform:translateY(30%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
            <h5 class="title-3 <?= $text_color; ?>"><?= $title; ?></h5>
            <h6 class="desc"><?= $subtitle; ?></h6>
          </article>
          <?php
            endwhile;
          endif;
          ?>
        </div>

        <?php if(!empty($secondary_desc)): ?>
        <div class="col-sm-12 col-md-8 ml-auto mr-auto">
          <div class="secondary-desc desc pt2 tc" data-bottom-top="opacity:0; transform:translateY(30%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
            <?= $secondary_desc; ?>
          </div>
        </div>
        <?php endif; ?>

      </div>
    </div>
  </div>
</div><!-- .inner-section-2 -->
