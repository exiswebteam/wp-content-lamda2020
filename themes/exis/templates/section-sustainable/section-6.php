<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$desc = get_sub_field('description');
?>
<div class="inner-section-6 on-viewport ovf-hidden pt4 pb1" data-fx="initiatives_section">

  <header class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-10 ml-auto mr-auto">
        <h4 class="blue-light mb4 x-s1-1" data-bottom-top="opacity:0; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h4>
      </div>
    </div>
  </header>

  <?php if(!empty($desc)): ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-10 col-lg-8 ml-auto mr-auto">
        <div class="desc tc mb2 x-s1-2" data-bottom-top="opacity:0; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);">
          <?= $desc; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>


  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-10 ml-auto mr-auto">
        <div class="row">
          <?php
          if( have_rows('add_ngos') ):
            $count_ngos = 0;
            while ( have_rows('add_ngos') ) : the_row();
            $icon = get_sub_field('icon');
            $desc = get_sub_field('description');
          ?>
          <div class="col-sm-6 col-md-3 item tc mb3 x-op-<?= $count_ngos++; ?>">
            <?php if(!empty($icon)): ?>
            <img class="mb1" src="<?= $icon['url']; ?>" style="display: inline-block;" alt="<?php the_title(); ?>" data-bottom-top="opacity:0; transform:translateY(50%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
            <?php endif; ?>

            <div class="wrap-desc desc" data-bottom-top="opacity:0.5; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);">
              <?= $desc; ?>
            </div>
          </div>
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</div><!-- .inner-section-6-->
