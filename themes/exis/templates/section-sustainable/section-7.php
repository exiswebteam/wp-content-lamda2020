<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$desc = get_sub_field('description');
?>
<div class="inner-section-7 on-viewport ovf-hidden pt3 pb4" data-fx="values_section">
  <header class="container" style="display: none;">
    <h3 class="section-subtitle blue x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h3>
  </header>

  <div class="container" style="display: none;">
    <div class="row">
      <div class="col-sm-12 col-md-6 ml-auto mr-auto">
        <div class="desc tc mb3 x-s1-2" data-bottom-top="transform: translateX(50%);" data-center-center="transform:translateX(0);">
          <?= $desc; ?>
        </div>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">
      <div class="col-item col-sm-12 col-md-11 col-lg-10 ml-auto mr-auto">
        <div class="row">
          <?php
          if( have_rows('add_values') ):
            $count_values = 0;
            while ( have_rows('add_values') ) : the_row();
            $icon = get_sub_field('icon');
            $desc = get_sub_field('description');
          ?>
          <div class="col-md-4 item mb2 x-op-<?= $count_values++; ?>">
            <?php if(!empty($icon)): ?>
            <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
            <?php endif; ?>
            <div class="wrap-desc desc" data-bottom-top="opacity:0.4; transform:translateY(50%);" data-center-center="opacity: 1; transform:translateY(0);">
              <?= $desc; ?>
            </div>
          </div>
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
</div><!-- .inner-section-7 -->
