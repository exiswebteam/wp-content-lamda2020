<?php
//SUSTAINABLE section
$title = get_sub_field('title');
$image1 = get_sub_field('image_1');
$image2 = get_sub_field('image_2');
$image3 = get_sub_field('image_3');
$desc = get_sub_field('description');
$desc2 = get_sub_field('description_2');
$desc3 = get_sub_field('description_3');
?>

<div class="inner-section-8 on-viewport ovf-hidden pt4 pb2" data-fx="content_section">

  <div class="content_section-1st mb2">
    <div class="container">
      <div class="row">
        <div class="col" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
          <h2 class="title-2 green2"><?= $title; ?></h2>
        </div>
        <div class="col" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
          <img src="<?= $image1['url']; ?>" alt="<?= $title; ?>"/>
        </div>
      </div>
    </div>
  </div>

  <div class="content_section-2nd ovf-hidden">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7">
          <div class="desc mb4" data-bottom-top="opacity:0.2; transform:translateX(-40%);" data-center-center="opacity: 1; transform:translateY(0);">
            <?= $desc; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="content-repeater-wrap pt3 pb4 mb4">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-5 col-lg-4 ml-auto mr-auto">

          <div class="slideshow-text" data-fade="false">
            <a href="#prev" class="arrow-prev green2"><i class="fa green2 fa-chevron-left" aria-hidden="true"></i></a>
            <a href="#next" class="arrow-next green2"><i class="fa green2 fa-chevron-right" aria-hidden="true"></i></a>
            <?php
            if( have_rows('slideshow') ):
              $count_add_icons = 0;
              while ( have_rows('slideshow') ) : the_row();
              $slide_desc = get_sub_field('description');
              $item_counter = $count_add_icons++;
            ?>
            <div class="slide-item">
              <article class="item tc mb3 op-<?= $item_counter; ?>">
                <div class="wrap-desc black desc">
                  <?= $slide_desc; ?>
                </div>
              </article>
            </div>
            <?php
              endwhile;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="content_section-3rd pb2 mb4">
    <div class="container">
      <div class="row">
        <div class="col" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
          <img src="<?= $image2['url']; ?>" class="mb1" alt="<?= $title; ?>"/>
        </div>
        <div class="col" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
          <?= $desc2; ?>
        </div>
      </div>
    </div>
  </div>

  <div class="content_section-4th pt4 pb4 mt4">
    <div class="container">
      <div class="row">
        <div class="col col-left" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
          <?= $desc3; ?>
        </div>
        <div class="col col-right" data-bottom-top="opacity:0; transform:translateY(10%) scale(0);" data-center-center="opacity: 1; transform:translateY(0) scale(1);">
          <img src="<?= $image3['url']; ?>" alt="<?= $title; ?>"/>
        </div>
      </div>
    </div>
  </div>

</div>
