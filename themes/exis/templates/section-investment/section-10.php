
<div class="inner-section-10 on-viewport pb4 pt4" data-fx="asset">
  <?= get_sub_field('video'); ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 tc col-md-10 col-lg-9 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(-5%);">
        <h2 class="title-3 white x-op-0 mb4"><?= get_sub_field('title'); ?></h2>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="charts-wrap on-viewport">
      <?php
      if( have_rows('charts_data') ):
          while ( have_rows('charts_data') ) : the_row();

              if( get_row_layout() == 'charts' ):
                  get_template_part('templates/section-investment/asset-charts/charts-data');

              elseif( get_row_layout() == 'chart_nav' ):
                get_template_part('templates/section-investment/asset-charts/charts-nav');

              endif;

          endwhile;
      endif;
      ?>
    </div>
  </div>
</div>
