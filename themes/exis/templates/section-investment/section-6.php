<div class="inner-section-6 on-viewport" data-fx="slideshow_image">

  <div class="slideshow-content-top">

    <?php
    if( have_rows('slideshow_repeater') ):
      echo '<div class="slideshow-text">';
      ?>
      <a class="arrow-prev arrow"><i class="fa white fa-chevron-left" aria-hidden="true"></i></a>
      <a class="arrow-next arrow"><i class="fa white fa-chevron-right" aria-hidden="true"></i></a>
      <?php
      while ( have_rows('slideshow_repeater') ) : the_row();
      $slide = get_sub_field('slide');

      if(!empty($slide)): ?>
      <div class="slide-item">
        <img class="slide-img" src="<?= $slide['url']; ?>" alt="<?php the_title(); ?>">
      </div>
      <?php
      endif;

      endwhile;
      echo '</div>';
    endif;
    ?>
  </div>

  <div class="slideshow-content-bottom">
    <div class="container ovf-hidden">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-9 tc ml-auto mr-auto">
          <article class="description ovf-hidden">
            <div class="wrap" data-bottom-top="opacity:0; transform:translateY(20%);" data-center-center="opacity: 1; transform:translateY(0);">
              <h4 class="tc title black"><?= get_sub_field('title'); ?></h4>
              <div class="desc tc">
                <?= get_sub_field('description'); ?>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>

</div>
