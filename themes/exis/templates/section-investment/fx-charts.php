<div class="container">
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-10 ml-auto mr-auto">
      <div class="inverstment-chart-wrap on-viewport">
        <?php
        if( have_rows('charts_data') ):
          while ( have_rows('charts_data') ) : the_row();

            if( get_row_layout() == 'chart' ):
                get_template_part('templates/section-investment/charts/chart');

            elseif( get_row_layout() == 'chart_nav_year' ):
                get_template_part('templates/section-investment/charts/charts-nav');

            endif;

          endwhile;
        endif;
        ?>
      </div>
    </div>
  </div>
</div>
