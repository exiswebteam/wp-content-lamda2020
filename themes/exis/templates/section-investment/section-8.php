<?php
$graphic1 = get_sub_field('graphic_1');
$graphic2 = get_sub_field('graphic_2');
$graphic3 = get_sub_field('graphic_3');
$desc1 = get_sub_field('description_1');
$desc2 = get_sub_field('description_2');
$desc3 = get_sub_field('description_3');
$desc4 = get_sub_field('description_4');
?>
<div class="square-up-right">
  <div class="inner-section-8 on-viewport pb4 pt4 ovf-hidden" data-fx="covid_19">


    <div class="container">
      <div class="row">
        <div class="col-sm-12 tc col-md-10 col-lg-8 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
          <h2 class="title-3 tc blue-light mb1"><?= get_sub_field('title'); ?></h2>
          <div class="desc mb3 white x-op-0">
            <?= get_sub_field('description'); ?>
          </div>
        </div>
      </div>
    </div>

    <?php if(!empty($graphic1)): ?>
    <div class="graphic tc mb2" data-bottom-top="transform: translateY(20%);" data-center-center="transform: translateY(0%);">
      <img src="<?= $graphic1['url']; ?>" alt="<?= get_sub_field('title'); ?>">
    </div>
    <?php endif; ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-10 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
          <div class="desc desc-400 mb2 white x-op-0">
            <?= get_sub_field('description_2'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-10 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
          <div class="desc tc mb4 white x-op-0">
            <?= get_sub_field('description_3'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-11 col-lg-11 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
          <h3 class="title-3 tc mb4 blue-light x-op-0 mb4"><?= get_sub_field('title_2'); ?></h3>
        </div>
      </div>
    </div>

    <?php if(!empty($graphic2)): ?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-9 col-lg-8 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
          <div class="tc graphic mb4" data-bottom-top="transform: translateY(20%);" data-center-center="transform: translateY(0%);">
            <img src="<?= $graphic2['url']; ?>" alt="<?= get_sub_field('title'); ?>">
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if(!empty($graphic3)): ?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-6 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
          <div class="row">
            <div class="col pt2 tr">
              <img src="<?= $graphic3['url']; ?>" alt="<?= get_sub_field('title'); ?>">
            </div>
            <div class="col white">
              <?= get_sub_field('number'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>

  </div>
</div>
