<div class="inner-section-9 on-viewport pt4 pb4" data-fx="retail_bond">

  <div class="container">
    <div class="row">
      <div class="col-sm-12 tc col-md-10 col-lg-9 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(-5%);">
        <h2 class="title-3 blue-light x-op-0 mb1"><?= get_sub_field('title'); ?></h2>
        <div class="desc mb3 x-op-0">
          <?= get_sub_field('description'); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <?php
      if( have_rows('add_icons') ):
        while ( have_rows('add_icons') ) : the_row();
        $title = get_sub_field('title');
        $icon = get_sub_field('icon');
        $desc = get_sub_field('description');
      ?>
      <div class="col-sm-6 col-md-3">
        <div class="icon-wrapper x-op-0 mb4">
          <article class="item x-op-0">
            <?php if(!empty($icon)): ?>
            <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="transform: scale(0);" data-center-center="transform: scale(1);">
            <?php endif; ?>
            <h5 class="title-3 blue" data-bottom-top="opacity: 0.4; " data-center-center="opacity: 1;"><?= $title; ?></h5>
            <div class="wrap-desc blue-light" data-bottom-top="opacity: 0.4;" data-center-center="opacity: 1;">
              <?= $desc; ?>
            </div>
          </article>
        </div>
      </div>
      <?php
        endwhile;
      endif;
      ?>
    </div>
  </div>

</div><!-- .inner-section-9 -->
