<?php
$val = get_sub_field('value');
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
?>

<div class="chart-item op-0">
  <div class="chart-wrap">
    <p class="tc blue-light top-title"><?= $title; ?></p>
    <div class="counter tc title-3 blue-light bold">
      <?= $val; ?>
    </div>
    <ul>
    <?php
    if( have_rows('add_chart_bar') ):
      while ( have_rows('add_chart_bar') ) : the_row();
      $color = get_sub_field('color');
      $val = get_sub_field('value');
      $counter = get_sub_field('counter');
      $bar_val = get_sub_field('bar_value');
      $bar_width = (!empty($val))? ((int)$val) * 1.6 : 100;
    ?>
    <li>
      <div class="bar" style="background-color: <?= $color; ?>; height: <?= $bar_width; ?>px;" data-val="<?= $val; ?>">
        <div class="val-2">
          <?= $bar_val; ?>
        </div>
      </div>
    </li>
    <?php
      endwhile;
    endif;
    ?>
    </ul>
    <p class="tc bold bottom-title">
      <?= $subtitle; ?>
    </p>
  </div>
</div>
