

<div class="chart-item">
  <div class="chart-wrap">
    <ul>
    <?php
    if( have_rows('add_data') ):
      while ( have_rows('add_data') ) : the_row();
      $color = get_sub_field('color');
      $val = get_sub_field('value');
      $title = get_sub_field('title');
      $title_top = get_sub_field('title_top');
      $year = get_sub_field('year');
    ?>
    <li>
      <span class="title-top blue"><?= $title_top; ?></span>
      <div class="bar" style="background-color: <?= $color; ?>; height: <?= $title * 0.164; ?>px;" data-val="<?= $title; ?>"><?= $val; ?></div>
      <div class="year-item"><?= $year; ?></div>
    </li>
    <?php
      endwhile;
    endif;
    ?>
    </ul>
  </div>
</div>
