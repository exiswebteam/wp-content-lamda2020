<?php
$stock_chart_svg = get_sub_field('graphic');
?>


<div class="inner-section-11 on-viewport pt4 pb4" data-fx="share_price_performance">

  <div class="container">
    <div class="row">
      <div class="col-sm-12 tc col-md-10 col-lg-8 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateY(30%);" data-center-center="opacity: 1; transform: translateY(0%);">
        <h2 class="title-3 tc blue-light mb1"><?= get_sub_field('title'); ?></h2>
        <div class="desc mb3 x-op-0">
          <?= get_sub_field('description'); ?>
        </div>
      </div>
    </div>
  </div>


  <div class="ovf-hidden">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
          <div class="stock-chart on-viewport" data-bottom-top="opacity:0; transform:translateX(10%);" data-center-center="opacity: 1; transform:translateX(0);">
            <?php
            //SVG IMAGE
            if(!empty($stock_chart_svg)):
              echo $stock_chart_svg;
            endif;
            ?>
            <nav class="stock-chart-nav">
              <?php
              //Stock value
              if( have_rows('stock_data') ):
                while ( have_rows('stock_data') ) : the_row();
                  $color = get_sub_field('color');
                  $desc = get_sub_field('description');
                  ?>
                  <div class="item" style="background-color: <?= $color; ?>;">
                    <span class="text"><?= $desc; ?></span>
                  </div>
                <?php
                endwhile;
              endif;
              ?>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div><!--.skrollr-container-->

</div>
