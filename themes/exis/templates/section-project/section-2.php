<?php
//PROJECT section
$title = get_sub_field('title');
$text_color = get_sub_field('text_color');
$bg_color = get_sub_field('background_color');
$desc = get_sub_field('description');
$cols = get_sub_field('columns');
$bg_image = get_sub_field('background_image');
$align = get_sub_field('align');
$bg_img_css = (!empty($bg_image))? ' background-image: url('.$bg_image['url'].');': '' ;
$bg_class = (!empty($bg_image))? 'bg-true': 'bg-false' ;
$icon_top = get_sub_field('icon_top');
?>

<div class="inner-section-2 on-viewport x-ovf-hidden pt2 pb1 bg-cover add-icon-<?= $icon_top; ?> <?= $bg_class; ?>" data-fx="icons" style="background-color: <?= $bg_color; ?>;<?= $bg_img_css; ?>">
  <?php
  if( have_rows('slideshow_repeater') ):
    echo '<div class="slideshow-init overlap-container-back" data-fade="true">';

    while ( have_rows('slideshow_repeater') ) : the_row();
    $slide = get_sub_field('slide');

    if(!empty($slide)): ?>
    <div class="slide-item bg-cover" style="background-image: url(<?= $slide['url']; ?>);">
      <!--img class="slide-img" src="<?= $slide['url']; ?>" alt="<?php the_title(); ?>"-->
    </div>
    <?php
    endif;

    endwhile;
    echo '</div>';
  endif;
  ?>

  <div class="container overlap-container-front">
    <div class="row">
      <div class="col-sm-12 col-md-11 col-lg-8 ml-auto mr-auto">
        <h3 class="title-3 tc mb1 x-op-0 <?= $text_color; ?>" data-bottom-top="opacity:0; transform:translateY(-20px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title; ?></h3>
        <div class="desc tc mb2 x-op-0" data-bottom-top="opacity:0; transform:translateY(40px);" data-center-center="opacity: 1; transform:translateY(0);">
          <?= $desc; ?>
        </div>

        <div class="icons-repeater-wrap ovf-hidden align-<?= $align; ?> columns-<?= $cols; ?>">
          <?php
          if( have_rows('add_icons') ):
            $count_add_icons = 0;
            while ( have_rows('add_icons') ) : the_row();
            $title2 = get_sub_field('title');
            $icon = get_sub_field('icon');
            $desc2 = get_sub_field('description');
          ?>
          <article class="item mb4 x-op-<?= $count_add_icons++; ?>">
            <?php if(!empty($icon)): ?>
            <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="opacity:0; transform:scale(0.1);" data-center-center="opacity: 1; transform:scale(1);">
            <?php endif; ?>
            <h5 class="title-3 <?= $text_color; ?>" data-bottom-top="opacity:0; transform:translateY(20px);" data-center-center="opacity: 1; transform:translateY(0);"><?= $title2; ?></h5>
            <div class="wrap-desc desc" data-bottom-top="opacity:0; transform:translateY(20px);" data-center-center="opacity: 1; transform:translateY(0);">
              <?= $desc2; ?>
            </div>
          </article>
          <?php
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>

</div><!--.inner-section-2-->
