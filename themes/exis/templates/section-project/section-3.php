<?php
//PROJECT section
$title = get_sub_field('title');
$bg_image = get_sub_field('background_image');
$bg_color = get_sub_field('background_color');
$desc = get_sub_field('description');
$svg = get_sub_field('svg_graph');
$animations = get_sub_field('show_animations');
?>
<div class="inner-section-3 on-viewport" data-fx="smart_city" style="background-color: <?= $bg_color; ?>;">

  <div class="top-section-header cover pt4 pb1 ovf-hidden" style="background-image: url(<?= $bg_image['url']; ?>);">

    <h3 class="title-3 blue tc x-s1-1 mb2" data-bottom-top="opacity:0; transform:scale(0.4);" data-center-center="opacity: 1; transform:scale(1);"><?= $title; ?></h3>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-6 ml-auto mr-auto">
          <div class="desc tc black x-s1-2 mb4" data-bottom-top="opacity:0; transform:translateY(80px);" data-center-center="opacity: 1; transform:translateY(0);">
            <?= $desc; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="city-svg-animation show-animation<?= $animations; ?> tc x-s1-3 on-viewport"  data-bottom-top="opacity:.4; transform:translateY(90%);" data-center-center="opacity: 1; transform:translateY(0);">
      <div class="bg-city" style="background-image: url(<?= $svg['url']; ?>);">
        <span class="icon icon-1 up-down scroll-to" data-id="#city-item-4" data-offset="20"></span>
        <span class="icon icon-2 up-down scroll-to" data-id="#city-item-2" data-offset="20"></span>
        <span class="icon icon-3 up-down scroll-to" data-id="#city-item-1" data-offset="20"></span>
        <span class="icon icon-4 up-down scroll-to" data-id="#city-item-2" data-offset="20"></span>
        <span class="icon icon-5 up-down scroll-to" data-id="#city-item-6" data-offset="20"></span>
        <span class="icon icon-circle one"></span>
        <span class="icon icon-circle two"></span>
        <span class="icon icon-circle three"></span>
        <span class="icon icon-cross one"></span>
        <span class="icon icon-cross two"></span>
        <span class="icon icon-signal scroll-to" data-id="#city-item-2" data-offset="20"></span>
      </div>
    </div>
  </div>

  <div class="columns-repeater-wrap pt3 pb4">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-8 ml-auto mr-auto">
          
          <div class="slideshow-text" data-fade="false">
            <a href="#prev" class="arrow-prev"><i class="fa white fa-chevron-left" aria-hidden="true"></i></a>
            <a href="#next" class="arrow-next"><i class="fa white fa-chevron-right" aria-hidden="true"></i></a>
            <?php
            if( have_rows('add_columns') ):
              $count_add_icons = 0;
              while ( have_rows('add_columns') ) : the_row();
              $title = get_sub_field('title');
              $desc = get_sub_field('description');
              $item_counter = $count_add_icons++;
            ?>
            <div class="slide-item">
              <article id="city-item-<?= $item_counter; ?>" class="item tc mb3 op-<?= $item_counter; ?>">
                <h5 class="title blue title-3"><?= $title; ?></h5>
                <div class="wrap-desc black desc">
                  <?= $desc; ?>
                </div>
              </article>
            </div>
            <?php
              endwhile;
            endif;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .inner-section-3 -->
