<?php
//Hellinikon Project section
$post_id = get_the_ID();
$section = get_post_field( 'menu_order', $post_id);
$bg_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$section_title = get_the_title();
$secondary_title = get_field('title_secondary');
$section_subtitle = get_field('subtitle');
$section_subtitle2 = get_field('secondary_subtitle');
?>
<div class="square-up-left">
  <div class="inner-section-1 on-viewport x-ovf-hidden">
    <div class="header-wrap x-ovf-hidden">
      <hgroup class="x-container">
        <h3 class="section-title green-light x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px) scale(0.1);" data-center-center="opacity: 1; transform:translateY(0) scale(1);"><?= (!empty($secondary_title))? $secondary_title : $section_title; ?></h3>
        <h6 class="section-num white right x-s1-0" data-bottom-top="transform:translateY(80px) scale(0.2);" data-center-center="transform:translateY(10px) scale(1);">0<?= $section; ?></h6>
      </hgroup>

      <?php
      if( have_rows('slideshow_repeater') ):
        echo '<div class="slideshow-init">';

        while ( have_rows('slideshow_repeater') ) : the_row();
        $slide = get_sub_field('slide');

        if(!empty($slide)): ?>
        <div class="slide-item">
          <img class="slide-img" src="<?= $slide['url']; ?>" alt="<?php the_title(); ?>">
        </div>
        <?php
        endif;

        endwhile;
        echo '</div>';
      endif;
      ?>

      <div class="parallax-wrapper x-op-0">
        <div class="container" data-bottom-top="transform:translateY(20%); opacity:0;" data-center-center="transform:translateY(0); opacity:1;">
          <div class="row">
            <div class="col-xs-12 col-sm-12 tc col-md-11 col-lg-11 ml-auto mr-auto">
              <div class="desc desc-parallax tc">
                <?= get_post_field('post_content', $post_id); ?>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div><!--.inner-section-1-->


<div class="inner-section-1-bottom on-viewport pt4 pb4 ovf-hidden">
  <?php if(!empty($section_subtitle2)): ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 tc col-md-10 col-lg-7 ml-auto mr-auto"  data-bottom-top="opacity:0; transform: translateX(-30%);" data-center-center="opacity: 1; transform: translateX(0);">
        <h3 class="title-3 blue x-op-0"><?= $section_subtitle2; ?></h3>
        <div class="desc mb3 x-op-0">
          <?= get_field('description'); ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>


  <div class="container">
    <div class="row">
      <div class="col-sm-11 col-md-8 col-lg-10 ml-auto mr-auto">
        <div class="row justify-content-sm-center">

              <?php
              if( have_rows('add_icons') ):
                while ( have_rows('add_icons') ) : the_row();
                $title = get_sub_field('title');
                $icon = get_sub_field('icon');
                $desc = get_sub_field('description');
              ?>
              <div class="col-sm-6 col-md-6 col-lg-3 ml-auto mr-auto">
                <div class="icon-wrapper x-op-0">
              <article class="item x-op-0">
                <?php if(!empty($icon)): ?>
                <img class="mb1" src="<?= $icon['url']; ?>" alt="<?php the_title(); ?>" data-bottom-top="transform: scale(0);" data-center-center="transform: scale(1);">
                <?php endif; ?>
                <h5 class="title-3 blue" data-bottom-top="opacity: 0.4; " data-center-center="opacity: 1;"><?= $title; ?></h5>
                <div class="wrap-desc blue-light" data-bottom-top="opacity: 0.4;" data-center-center="opacity: 1;">
                  <?= $desc; ?>
                </div>
              </article>
            </div>
          </div>
          <?php
            endwhile;
          endif;
          ?>

        </div>
      </div>
    </div>
  </div>
</div>

</div><!-- .square-up-left -->
