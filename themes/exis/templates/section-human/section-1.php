<?php
//HUMAN Capital section
$post_id = get_the_ID();
$section = get_post_field( 'menu_order', $post_id);
$section_title = get_the_title();
$secondary_title = get_field('title_secondary');
$section_subtitle = get_field('subtitle');
?>

<div class="square-up-right square-blue-light">
  <div class="inner-section-1 bg-blue x-ovf-hidden on-viewport" data-fx="">

    <div class="header-wrap">

      <hgroup class="container">
        <h3 class="section-title blue x-s1-1" data-bottom-top="opacity:0; transform:translateY(40px) scale(0.1);" data-center-center="opacity: 1; transform:translateY(0) scale(1);"><?= (!empty($secondary_title))? $secondary_title : $section_title; ?></h3>
        <h6 class="section-num left x-s1-0" data-bottom-top="transform:translateY(80px) scale(0.2);" data-center-center="transform:translateY(10px) scale(1);">0<?= $section; ?></h6>
      </hgroup>

      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-10 col-lg-7 ml-auto mr-auto">
            <div class="desc white x-s1-3 pb4" data-bottom-top="opacity:0; transform:translateX(-40%);" data-center-center="opacity: 1; transform:translateX(0);">
              <?= get_post_field('post_content', $post_id); ?>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div><!--.inner-section-1-->
</div>
