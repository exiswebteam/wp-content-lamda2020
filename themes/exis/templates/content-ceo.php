<?php
$post_id = get_the_ID();
$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$title = get_the_title();
$secondary_title = get_field('title_secondary');
?>

<div class="picture-card-section on-viewport">

  <div class="picture-card width-30-70">

    <div class="picture-card-img-wrap ovf-hidden">
      <?php if(!empty($img)): ?>
      <img class="x-a1" src="<?= $img[0]; ?>" alt="<?= $title; ?>" data-bottom-top="opacity:1; transform:translateX(-30%);" data-center-center="opacity: 1; transform:translateX(0);">
      <?php endif; ?>
    </div>

    <div class="picture-card-content-wrap">

      <div class="picture-card-content-inner">

        <h2 class="title x-a2" data-bottom-top="opacity:0; transform:translateY(10%);" data-center-center="opacity: 1; transform:translateY(0);"><?= (!empty($secondary_title))? $secondary_title : $title; ?></h2>
        <div class="content desc x-a3" data-bottom-top="opacity:0; transform:translateY(15%);" data-center-center="opacity: 1; transform:translateY(0);">
          <?= get_post_field('post_content', $post_id); ?>
        </div>

        <div class="more-content desc hide" id="read-more-ceo">
          <?= get_field('ceo_more_content'); ?>
        </div>


        <div class="picture-card-cta tr cta-btn-wrap a4">
          <a href="#read-more-ceo" class="read-more tc" data-less="<?= get_field('button_less'); ?>" data-more="<?= get_field('button_more'); ?>"><?= get_field('button_more'); ?></a>
        </div>

      </div>

    </div><!-- end of picture-card-content-wrap -->

  </div><!-- end of picture-card -->


</div><!-- end of picture-card-section -->

<div class="financial-content-wrap at-a-glance">
  <?php
  $video_bg = get_field('html5_video');
  $subtitle = get_field('subtitle');
  ?>

  <header class="top-title-wrap">
    <div class="container">
      <h2 class="blue title fw300 fsize42"><?= $subtitle; ?></h2>
    </div>
  </header>

  <?php
  $video_bg = get_field('html5_video');

  if(!empty($video_bg)):
   echo $video_bg;
  endif;
  ?>

  <?php
  //Fx content area
  if( have_rows('add_section') ):

      while ( have_rows('add_section') ) : the_row();

      if( get_row_layout() == 'numerical_figures' ): get_template_part('templates/section-financial/section-2');

      //Close
      endif;

      endwhile;

    else :
      // no flexible content

  endif;

  ?>
</div><!-- .financial-content-wrap -->
