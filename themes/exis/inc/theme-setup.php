<?php
/*====================================================================
  Remove Comments
====================================================================*/
add_action( 'admin_init', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

/*====================================================================
   Dequeue - Remove parent themes stylesheet and scripts
====================================================================*/
function kyntheum_theme_remove_scripts()
{
  // Get the theme data
  $the_theme = wp_get_theme();

  //Dequeue understrap
  wp_dequeue_script( 'understrap-scripts' );
  wp_deregister_script( 'understrap-scripts' );

  //Dequeue gutenberg & WP default styles
  wp_dequeue_style( 'wp-block-library' );
  wp_dequeue_style( 'wc-block-style' );
  wp_dequeue_style( 'understrap-styles' );
  wp_deregister_style( 'understrap-styles' );
  wp_deregister_script( 'wp-embed' );

  //Dequeue contact form 7 and add sass - _plugin-cf7.scss
  wp_deregister_style( 'contact-form-7' );

  //GDPR - _plugin-gdpr.scss
  wp_dequeue_style('cookie-law-info');
	wp_dequeue_style('cookie-law-info-public');
	wp_dequeue_style('cookie-law-info-gdpr');

  //style.css
  wp_enqueue_style( 'exis', get_stylesheet_directory_uri() . '/style.css', array(), $the_theme->get( 'Version' ) );

  /* SASS - GULP - Bootstrap and main css */
  wp_enqueue_style( 'main-css-compiled-gulp', get_stylesheet_directory_uri() . '/dist/css/main-compiled.css', array(), $the_theme->get( 'Version' ) );

  //jQuery - deregister and use 2.2.4 from cdn
  wp_deregister_script('jquery');
  wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), null, true);

  //Gsap - tweenmax
  wp_enqueue_script('gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.6/gsap.min.js', array('jquery'), null, true);

  //Charts
  //wp_enqueue_script('charts', 'https://code.highcharts.com/highcharts.js', array('jquery'), null, true);

  /* GULP - JS  */
  wp_enqueue_script( 'main-js-compiled-gulp', get_stylesheet_directory_uri() . '/dist/js/main.js', array('jquery'), $the_theme->get( 'Version' ), true );
}

add_action( 'wp_enqueue_scripts', 'kyntheum_theme_remove_scripts', 20 );


/*====================================================================
   REMOVE HEADER SCRIPTS - Clean head
====================================================================*/
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action('wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'index_rel_link' ); // index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.


//Remove SVG css and added with SASS
remove_action( 'wp_enqueue_scripts', 'bodhi_svgs_frontend_css' );



/*====================================================================
   Widgets
====================================================================*/
function wpb_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer column 1',
		'id'            => 'footer-column-1',
		'before_widget' => '<div id="%1$s" class="foot-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="foot-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer column 2',
		'id'            => 'footer-column-2',
		'before_widget' => '<div id="%1$s" class="foot-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="foot-title">',
		'after_title'   => '</h2>',
	) );

}
//add_action( 'widgets_init', 'wpb_widgets_init' );



/*====================================================================
   Remove Logo
====================================================================*/
function annointed_admin_bar_remove() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render','annointed_admin_bar_remove', 0);



/*====================================================================
  Body classes
====================================================================*/
add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');

function my_mce_before_init_insert_formats($init_array)
{
    $style_formats = array(
        array(
            'title' => 'Color Red',
            'inline' => 'span',
            'classes' => 'text-color-red'
        ),
        array(
            'title' => 'Color Blue',
            'inline' => 'span',
            'classes' => 'text-color-blue'
        ),
        array(
            'title' => 'Blue Light',
            'inline' => 'span',
            'classes' => 'blue-light'
        ),
        array(
            'title' => 'Color Primary',
            'inline' => 'span',
            'classes' => 'text-color-blue'
        ),
        array(
            'title' => 'Button',
            'inline' => 'span',
            'classes' => 'btn primary'
        ),
    );

    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}
