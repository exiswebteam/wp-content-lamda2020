<?php
/**
 * Template Name: Section - Key Assets
 *
 * @package exis
 */

defined( 'ABSPATH' ) || exit;

get_template_part('templates/section-header');
get_template_part('templates/content-investment');
get_template_part('templates/section-footer');
?>
